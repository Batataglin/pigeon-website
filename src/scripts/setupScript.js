/* Funtion to set the price of the pack chosen by the user
   the pack value is attributted on the dropbox item */
function setBundlePrice() {
    if (!document.getElementById("pa_car-selection").value.trim() || !document.getElementById("pa_select-bundle-or-track").value.trim()) {
        document.getElementById("lblBundlePrice").innerText = '0.00';
        handleBuyButtonDisabled(true);
        handleBuyFormDisabled(true);
    }
    else {
        var bundleValue = document.getElementById("pa_select-bundle-or-track").value;
        document.getElementById("lblBundlePrice").innerText = bundleValue;
        handleBuyButtonDisabled(false);
    }
}

/* Local function to enable the buy button based on the car selection
and the bundle dropbox */
function handleBuyButtonDisabled(disabled) {

    var button = document.getElementById("btnBuyNow");
    button.disabled = disabled;
    if (disabled) {
        button.classList.add("disabled");
    } else {
        button.classList.remove("disabled");
    }
}

function handleBuyFormDisabled(disabled) {
    if (disabled) {
        document.getElementById("dvPurchaseForm").style.display = "none";
    }
    else {
        document.getElementById("dvPurchaseForm").style.display = "block";
    }
}

function buy() {
    // Using const because we are not going to alter this variable
    // Get the value from its ID
    // Trim the string to remove any empty spaces
    // Convert the text to lowercase.
    const email = document.getElementById("iptEmail").value.trim().toLowerCase();
    const name = document.getElementById("iptName").value.trim().toLowerCase();

    if (checkForEmptyFields()) {
        alert("Please, fill in all fields on the form.");
    }
    else if (!validateName(name)) {
        alert("Please, fill in a valid name.");
    }
    else if (!validateEmail(email)) {
        alert("Please, fill in a valid email address.");
    }
    else {
        clearInformation();
        alert("Thank you for purchase our setup! \n A message was sent to your email to proceed the payment.");
    }

    function checkForEmptyFields() {
        // Access the instance of the html document, get the element by the ID, and
        // apply trim to remove any empty space from its value, than validate if
        // any element is empty, return false
        return (
            !document.getElementById("iptName").value.trim() ||
            !document.getElementById("iptEmail").value.trim()
        );
    }

    function validateName(name) {
        // Trim the string to remove any empty spaces
        name = name.trim();

        /* Breaking down the regex:
            ^[a-zA-Z] = Matches characters that are letters in the local part.
            {2,60}$ = Limit the minimum of 2 characters up to 60 charactes.
        */
        const nameRegex = /^[a-zA-Z]{2,60}$/;

        // test the parameter string with the REGular EXpression (Regex)
        return nameRegex.test(name);
    }

    function validatePhoneNumber(phone) {
        // Trim the string to remove any empty spaces
        phone = phone.trim();

        /* Breaking down the regex:
            ^[0-9] = Matches characters that are integer numbers in the local part.
            {9,10}$ = Limit the minimum of 9 characters up to 10 charactes.
        */
        const phoneRegex = /^[0-9]{9,10}$/;

        // test the parameter string with the REGular EXpression (Regex)
        return phoneRegex.test(phone);
    }

    /* The function takes a string uses a regular expression to check if the email
       matches the pattern. The regular expression ^[a-z0-9_.-]+@[a-z0-9.-]+\.[a-z]{2,}$
       ensures that the email has a local part, an "@" symbol, and a domain part, separated by a dot. */
    function validateEmail(email) {
        /* Breaking down the regex:
            ^[a-z0-9_.-]+ = Matches one or more characters that are either letters and digits, underscore, 
            dot, or hyphen in the local part.
            @ = Matches the "@" symbol.
            [a-z0-9.-]+ = Matches one or more characters that are either letters and digits, dot , or hyphen 
            in the domain part.
            \. = Matches a literal dot "." character.
            [a-zA-Z]{2,}$ = Matches two or more alphabetic characters at the end of the string, representing
            the top-level domain.
        */
        const emailRegex = /^[a-z0-9_.-]+@[a-z0-9.-]+\.[a-z]{2,}$/;

        // test the parameter string with the REGular EXpression (Regex)
        return emailRegex.test(email);
    }

    /* This function set null for each id on the setup from to clear the fields */
    function clearInformation() {
        // Access the instance of the html document, get the element by the ID
        // and set to an empty string to clear the input field.
        document.getElementById("iptName").value = "";
        document.getElementById("iptEmail").value = "";

    }

}